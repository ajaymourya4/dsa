package com.ajaymourya.patterns;

import java.util.HashSet;
import java.util.Set;

/**
 * Given an array of integers, find if the array contains any duplicates.
 * <p>
 * Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.
 * <p>
 * Example 1:
 * <p>
 * Input: [1,2,3,1]
 * Output: true
 * <p>
 * Example 2:
 * <p>
 * Input: [1,2,3,4]
 * Output: false
 */
class ContainsDuplicate {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> hashSet = new HashSet<Integer>();
        for (int num : nums) {
            if (hashSet.contains(num)) {
                return true;
            }
            hashSet.add(num);
        }
        return false;
    }
}

/**
 * Brute force Solution
 *
 * Iterate over the whole array for each element and check whether there is duplicate
 * Time -> O(N*N)
 * Space -> O(1)
 *
 * Sort the array and check the adjacent elements for duplicates
 * Time -> O(N*logN)
 * Space -> O(1)
 * =========================================
 * Optimal Solution
 *
 * Use a HashSet
 * Time -> O(N)
 * Space -> O(N)
 */
